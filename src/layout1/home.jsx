import React, { Component } from "react";
import Carousel from "./carousel";
import Content from "./content";
import Footer from "./footer";
import Header from "./header";

class Layout1 extends Component {
  render() {
    return (
      <div>
        <Header />
        <Carousel />
        <Content />
        <Footer/>
      </div>
    );
  }
}

export default Layout1;
