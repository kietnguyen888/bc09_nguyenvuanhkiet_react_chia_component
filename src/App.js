import './App.css';
import Layout1 from './layout1/home';
import Layout2 from './layout2/home';

function App() {
  return (
    <div className="App">
      {/* <Layout1/> */}
      <Layout2/>
    </div>
  );
}

export default App;
