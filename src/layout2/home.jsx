import React, { Component } from "react";
import Carousel from "./carousel";
import Header from "./header";
import Smartphone from "./smartphone";
import Laptop from "./laptop"
import Promotion from "./promotion";
class Layout2 extends Component {
  render() {
    return (
      <div>
        <Header />
        <Carousel/>
        <Smartphone/>
        <Laptop/>
        <Promotion/>
      </div>
    );
  }
}

export default Layout2;
